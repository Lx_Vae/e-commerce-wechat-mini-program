import { useShopCart } from "@/store/index"

// app.js
App({
  onLaunch() {
    useShopCart().get()
    .catch(error => {
      console.log(error);
      if (error.errMsg == 'getStorage:fail data not found') {
        useShopCart().set([])
      }
    })
  }
})
