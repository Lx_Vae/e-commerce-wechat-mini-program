import { useShopCart } from "@/store/index";
import Dialog from '@vant/weapp/dialog/dialog';

// pages/shop-cart/shop-cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    selectedList: [],
    totalPrice: 0,
    checkedAll: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.initData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    useShopCart().get()
    .then(res => {
      this.setData({
        goodsList: res.data,
        checkedAll: res.data.filter(g => ! g.checked).length == 0
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  initData() {
    useShopCart().get()
    .then(res => {
      this.setData({
        goodsList: res.data
      })
    })
    
  },
  selectChange(e) {
    const goods = e.detail.goods
    let newSelectedList
    let newTotalPrice
    if (e.detail.selected) {
      this.data.selectedList.push(goods)
      newSelectedList = this.data.selectedList
      newTotalPrice = Number(this.data.totalPrice) + Number(goods.price)
    } else {
      newSelectedList = this.data.selectedList.filter(g => {
        return g.name != goods.name
      })
      newTotalPrice = Number(this.data.totalPrice) - Number(goods.price)
    }
    const goodsList = this.data.goodsList.map(g => {
      if (g.productId == goods.productId) {
        return goods;
      }
      return g;
    })

    this.setData({
      goodsList: goodsList,
      totalPrice: newTotalPrice,
      selectedList: newSelectedList
    }, () => {
      useShopCart().set(goodsList)
    })
  },
  checkAll(e) {
    const goodsList = this.data.goodsList.map(goods => {
      goods.checked = e.detail
      return goods
    })
    if (e.detail) {
      this.setData({
        goodsList: goodsList,
        totalPrice: this.data.goodsList.reduce((prev, goods) => {
          prev += Number(goods.price)
          return prev
        }, 0),
        selectedList: this.data.goodsList,
        checkedAll: true
      }, () => {
        useShopCart().set(goodsList)
      })
    } else {
      this.setData({
        goodsList: goodsList,
        totalPrice: 0,
        selectedList: [],
        checkedAll: false
      }, () => {
        useShopCart().set(goodsList)
      })
    }
  },
  del(e) {
    const { position, name, instance } = e.detail;
    const goods = name
    if (position == 'right') {
      Dialog.confirm({
        message: '确定删除吗？',
      }).then(() => {
        console.log('del: ', goods);
        useShopCart().del(goods)
        .then(res => {
          useShopCart().get()
          .then(res => {
            if (goods.checked) {
              this.setData({
                goodsList: res.data,
                totalPrice: this.data.totalPrice - Number(goods.price),
                selectedList: this.data.selectedList.filter(g => g.productId != goods.productId) 
              }, () => {
                instance.close()
              })
            } else {
              this.setData({
                goodsList: res.data
              }, () => {
                instance.close()
              })
            }
          })
        })
      }).catch(() => {
        instance.close()
      })
    }
  }
})

/**
[
        {
          "checked": false,
          "shopId": "1000004123",
          "commentCount": "500000",
          "price": "2799.00",
          "good": "96",
          "shopName": "小米京东自营旗舰店",
          "name": "小米13 徕卡光学镜头 第二代骁龙8处理器 12+256GB 黑色 5G手机 澎湃OS SU7小米汽车互联 AI手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/241787/24/10650/61586/66603516Fcead3a3c/060b556bde50bee5.jpg",
          "customAttrList": [
            "第二代骁龙8",
            "256GB",
            "OLED直屏",
            "黑色",
            "6.36英寸"
          ],
          "customAttrListNew": [
            "第二代骁龙8",
            "256GB",
            "OLED直屏",
            "黑色",
            "6.36英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"外观\",\"saleValue\":\"黑色\",\"sequenceNo\":1},{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"12GB+256GB\",\"sequenceNo\":3},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"标准版\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "1000007143",
          "commentCount": "20000",
          "price": "6599.00",
          "good": "98",
          "shopName": "中国移动京东自营官方旗舰店",
          "name": "Apple iPhone 15 Pro (A3104) 128GB白色钛金属支持移动联通电信5G双卡双待手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/224160/6/11358/51490/659ce925F646a0447/ae22e8d9ba84299a.jpg",
          "customAttrList": [
            ""
          ],
          "customAttrListNew": [
            ""
          ],
          "beforeTitleUrl": "",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"颜色\",\"saleValue\":\"白色钛金属\",\"sequenceNo\":3},{\"dim\":2,\"saleName\":\"存储\",\"saleValue\":\"128G\",\"sequenceNo\":1},{\"dim\":3,\"saleName\":\"\",\"saleValue\":\"优惠一（购机立享 话费补贴）\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "1000468102",
          "commentCount": "20000",
          "price": "8599.00",
          "good": "98",
          "shopName": "中国移动手机京东自营官方旗舰店",
          "name": "Apple iPhone 15 Pro Max (A3108) 256GB 白色钛金属 支持移动联通电信5G【一级】",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/241769/30/5605/58965/65f40f5dF8971407c/2d2ec4535f6f03c0.jpg",
          "customAttrList": [
            ""
          ],
          "customAttrListNew": [
            ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"颜色\",\"saleValue\":\"白色钛金属\",\"sequenceNo\":3},{\"dim\":2,\"saleName\":\"存储\",\"saleValue\":\"256GB\",\"sequenceNo\":1},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"官方标配\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "10180819",
          "commentCount": "10000",
          "price": "2389.00",
          "good": "94",
          "shopName": "爱回收严选官方旗舰店",
          "name": "Apple 苹果 iPhone 14/13/12/11/X系列二手手机 颜色内存以质检报告为准 苹果 iPhone 13",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/190403/15/47600/66739/6662b0a6Fdfe3f9e7/58b194cd56eb4119.jpg",
          "customAttrList": [
            ""
          ],
          "customAttrListNew": [
            ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"苹果 iPhone 13\",\"sequenceNo\":25}]"
        },
        {
          "checked": false,
          "shopId": "1000004123",
          "commentCount": "200000",
          "price": "1529.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70E 天玑8300-Ultra小米澎湃OS 12GB+256GB影青 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/223810/11/34035/60899/665aecb0Fdb17401a/5d5f2db3f8ba1f90.png",
          "customAttrList": [
            "天玑8300-Ultra",
            "256GB",
            "OLED直屏",
            "绿色",
            "6.67英寸"
          ],
          "customAttrListNew": [
            "天玑8300-Ultra",
            "256GB",
            "OLED直屏",
            "绿色",
            "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"外观\",\"saleValue\":\"影青\",\"sequenceNo\":3},{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"12GB+256GB\",\"sequenceNo\":2},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"官方标配\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "1000004123",
          "commentCount": "50000",
          "price": "1699.00",
          "good": "98",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi Turbo 3 第三代骁龙8s 小米澎湃OS 12+256 冰钛 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/246967/9/10998/100508/6662c83aF3b792f82/754e854c11332585.jpg",
          "customAttrList": [
            "第三代骁龙8s",
            "256GB",
            "OLED直屏",
            "金色",
            "6.67英寸"
          ],
          "customAttrListNew": [
            "第三代骁龙8s",
            "256GB",
            "OLED直屏",
            "金色",
            "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"外观\",\"saleValue\":\"冰钛\",\"sequenceNo\":3},{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"12GB+256GB\",\"sequenceNo\":1},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"官方标配\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "1000004123",
          "commentCount": "500000",
          "price": "2299.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70 第二代骁龙8 澎湃OS 12GB+512GB 墨羽 红米5G手机 手机 SU7 小米汽车互联 AI手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/232020/12/20397/82819/6662ea58F8c8a6e66/f077634481939643.jpg",
          "customAttrList": [
            "第二代骁龙8",
            "512GB",
            "OLED直屏",
            "黑色",
            "6.67英寸"
          ],
          "customAttrListNew": [
            "第二代骁龙8",
            "512GB",
            "OLED直屏",
            "黑色",
            "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"外观\",\"saleValue\":\"墨羽\",\"sequenceNo\":1},{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"12GB+512GB\",\"sequenceNo\":3},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"官方标配\",\"sequenceNo\":1}]"
        },
        {
          "checked": false,
          "shopId": "1000004123",
          "commentCount": "200000",
          "price": "1529.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70E 天玑8300-Ultra小米澎湃OS 12GB+256GB晴雪 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/240597/33/9758/64660/6662b851F0e8e314e/4aa36860bbcc21a6.jpg",
          "customAttrList": [
            "天玑8300-Ultra",
            "256GB",
            "OLED直屏",
            "白色",
            "6.67英寸"
          ],
          "customAttrListNew": [
            "天玑8300-Ultra",
            "256GB",
            "OLED直屏",
            "白色",
            "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": "[{\"dim\":1,\"saleName\":\"外观\",\"saleValue\":\"晴雪\",\"sequenceNo\":2},{\"dim\":2,\"saleName\":\"版本\",\"saleValue\":\"12GB+256GB\",\"sequenceNo\":2},{\"dim\":3,\"saleName\":\"购买方式\",\"saleValue\":\"官方标配\",\"sequenceNo\":1}]"
        }
      ]
 */