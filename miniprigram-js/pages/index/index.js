// import { allGoodsData } from "@/utils/allGoodsData";

Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [],
    sidebarList: [],
    allGoodsData: new Map(),
    curCategoryList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.initDefaultData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    setTimeout(() => {
      wx.stopPullDownRefresh()
    }, 500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  initDefaultData() {
    let allGoodsData
    wx.request({
      url: 'http://115.159.49.90:8080/data',
      success: (res) => {
        console.log(res);
        allGoodsData = res.data

        let sidebarList = [...Object.keys(allGoodsData)]
        sidebarList = sidebarList.concat(sidebarList).concat(sidebarList)
        this.setData({
          swiperList: [
            'https://cdn.jsdelivr.net/gh/zrgzs/images@main/images/20240613090014.png',
            'https://imgcps.jd.com/img-cubic/creative_server_cia_jdcloud/v2/2000318/100112790948/FocusFullshop/CkRqZnMvdDEvMTU5OTk5LzEwLzQ3MTc2LzEzNzU4MS82NjU4ZDI3MEY4N2JkMGNhNC9kYzRkZjliMmY5OTMwODM5LnBuZxIJMy10eV8wXzU0MAI4vot6QhYKEuWNjuS4uuW5s-adv-eUteiEkRABQhAKDOmch-aSvOadpeS4tBACQhAKDOeri-WNs-aKoui0rRAGQgoKBuWKm-iNkBAHWKTrv_n0Ag/cr/s/q.jpg',
            'https://imgcps.jd.com/img-cubic/creative_server_cia_jdcloud/v2/2000367/100054291126/FocusFullshop/CkNqZnMvdDEvMTQ5NTc0LzM1LzQwMTM2LzQ0MDUyLzY2Njc1M2YzRjA1NGQwNGE0LzM3YjBjYTRiZGZkYThiNWIucG5nEgkyLXR5XzBfNTMwAjjvi3pCIAoc5ryr5q2l6ICF6JOd54mZL-aXoOe6v-iAs-acuhABQhAKDOeVheS6q-S8mOWTgRACQhAKDOeri-WNs-aKoui0rRAGQgcKA-aKohAHWLalzd30Ag/cr/s/q.jpg',
            'https://imgcps.jd.com/img-cubic/creative_server_cia_jdcloud/v2/2000322/100093918937/FocusFullshop/CkNqZnMvdDEvMTUxMDMwLzM3LzQyODI3LzQ2NDY3LzY2NWNjNTdhRmExY2ZhYjA0LzE3MGQxMzBjNjJhZDBlNGQucG5nEgkyLXR5XzBfNTMwATjCi3pCHQoZ5bCP57Gz6JOd54mZL-aXoOe6v-iAs-acuhABQhAKDOWlvei0p-axh-iBmhACQhAKDOeri-WNs-aKoui0rRAGQgoKBuS8mOi0qBAHWNn9v_D0Ag/cr/s/q.jpg'
          ],
          sidebarList: sidebarList,
          allGoodsData: allGoodsData,
          curCategoryList: allGoodsData[sidebarList[0]]
        })
      }
    })
  },
  siderHandler(e) {
    const index = e.currentTarget.dataset.index
    this.setData({
      curCategoryList: this.data.allGoodsData[this.data.sidebarList[index]]
    })
  },
  toGoodsDetailView(e) {
    const index = e.currentTarget.dataset.index
    const goodsData = this.data.curCategoryList[index]
    wx.navigateTo({
      url: `/pages/category-list/category-list?goodsData=${JSON.stringify(goodsData)}`
    })
  }
})