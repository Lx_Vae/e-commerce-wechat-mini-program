// pages/category-list/category-list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsData: {},
    val1: '0',
    opt1: [
      {text: '综合推荐', value: '0'},
      {text: '最新上架', value: '1'},
      {text: '价格最低', value: '2'},
      {text: '价格最高', value: '3'},
      {text: '评价最多', value: '4'}
    ],
    val2: '1',
    opt2: [
      {text: '211限时达', value: '0'},
      {text: '有货优先', value: '1'},
      {text: '货到付款', value: '2'},
      {text: '京宝国际', value: '3'},
      {text: '促销商品', value: '4'},
      {text: '配送全球', value: '5'}
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const goodsData = JSON.parse(options.goodsData)
    this.setData({
      goodsData: goodsData
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    this.setData({
        ['goodsData.items']: this.data.goodsData.items.concat(this.data.goodsData.items)
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  disableDropdown(status, callable) {
    callable(false)
  }
})