// pages/goods-detail/goods-detail.js
import { behavior as computedBehavior } from 'miniprogram-computed'
import Toast from '@vant/weapp/toast/toast'
import { useShopCart } from '@/store/index'

Page({
  behaviors: [computedBehavior],
  /**
   * 页面的初始数据
   */
  data: {
    goods: {},
    parameters: '',
    areaPopup: {
      visible: false,
      showValue: '',
      areaValue: '',
      options: [
        {
          text: '浙江省',
          value: '330000',
          children: [{ text: '杭州市', value: '330100' }],
        },
        {
          text: '江苏省',
          value: '320000',
          children: [{ text: '南京市', value: '320100' }],
        },
      ]
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const data = JSON.parse(options.data)
    if (!data.picList) {
      data.picList = [data.imageUrl]
    }
    this.setData({
      goods: data
    })
  },

  computed: {
    saleAttr(data) {
      if (data.goods.productId) {
        return '已选：' + data.goods.saleAttributes.map(attr => attr.saleValue).join(" / ")
      } else {
        return ''
      }

    },
    saleAttrCount(data) {
      if (data.goods.productId) {
        return data.goods.saleAttributes.length;
      } else {
        return 0
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },

  showAreaPopup() {
    this.setData({
      ['areaPopup.visible']: true
    })
  },
  closeAreaPopup() {
    this.setData({
      ['areaPopup.visible']: false
    })
  },
  finishAreaPopup(e) {
    const { selectedOptions, value } = e.detail;
    const showValue = selectedOptions
      .map((option) => option.text || option.name)
      .join('/');
    this.setData({
      ['areaPopup.showValue']: showValue,
      ['areaPopup.areaValue']: value,
    }, () => {
      this.closeAreaPopup()
    })
  },
  toShopCartView() {
    wx.switchTab({
      url: '/pages/shop-cart/shop-cart',
    })
  },
  addShopCart() {
    useShopCart().add(this.data.goods)
      .then(res => {
        Toast.success('添加成功')
      })
      .catch(error => {
        Toast.fail({
          message: error
        })
      })
  },
  buyNow() {
    Toast.success('恭喜您获得面单资格，本次将为您免单')
  }
})