export const useShopCart = () => ({
  id: 'shop-cart',
  async get() {
    return wx.getStorage({
      key: this.id
    })
  },
  async set(goodsList) {
    return wx.setStorage({
      key: this.id,
      data: goodsList
    })
  },
  async add(goods) {
    return this.get()
      .then((res) => {
        if (! res.data) {
          res.data = []
        }
        if (res.data.filter(g => g.productId == goods.productId).length) {
          return Promise.reject('商品已在\n购物车中~')
        }
        res.data.push(goods)
        return this.set(res.data)
      })
  },
  async del(goods) {
    return this.get()
      .then((res) => {
        const goodsList = res.data.filter(g => g.productId != goods.productId)
        return this.set(goodsList)
      })
  }
})