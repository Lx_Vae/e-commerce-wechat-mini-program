export const allGoodsData = new Map([
  ["热卖手机", [{
    "name": "iPhone",
    "image": "https://m.360buyimg.com/babel/jfs/t1/157461/30/33273/2172/646ed221F0e152a81/4e6de19f3cf7f066.jpg",
    "items": [
      {
          "productId": "100066989944",
          "commentCount": "20000",
          "price": "6599.00",
          "good": "98",
          "shopName": "中国移动京东自营官方旗舰店",
          "name": "Apple iPhone 15 Pro (A3104) 128GB白色钛金属支持移动联通电信5G双卡双待手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/224160/6/11358/51490/659ce925F646a0447/ae22e8d9ba84299a.jpg",
          "customAttrList": [
              ""
          ],
          "customAttrListNew": [
              ""
          ],
          "beforeTitleUrl": "",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "颜色",
                  "saleValue": "白色钛金属",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "存储",
                  "saleValue": "128G",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "",
                  "saleValue": "优惠一（购机立享 话费补贴）",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100068722463",
          "commentCount": "20000",
          "price": "8599.00",
          "good": "98",
          "shopName": "中国移动手机京东自营官方旗舰店",
          "name": "Apple iPhone 15 Pro Max (A3108) 256GB 白色钛金属 支持移动联通电信5G【一级】",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/241769/30/5605/58965/65f40f5dF8971407c/2d2ec4535f6f03c0.jpg",
          "customAttrList": [
              ""
          ],
          "customAttrListNew": [
              ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "颜色",
                  "saleValue": "白色钛金属",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "存储",
                  "saleValue": "256GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "10025627467594",
          "commentCount": "10000",
          "price": "2389.00",
          "good": "94",
          "shopName": "爱回收严选官方旗舰店",
          "name": "Apple 苹果 iPhone 14/13/12/11/X系列二手手机 颜色内存以质检报告为准 苹果 iPhone 13",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/190403/15/47600/66739/6662b0a6Fdfe3f9e7/58b194cd56eb4119.jpg",
          "customAttrList": [
              ""
          ],
          "customAttrListNew": [
              ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "苹果 iPhone 13",
                  "sequenceNo": 25
              }
          ]
      },
      {
          "productId": "100068431529",
          "commentCount": "20000",
          "price": "8599.00",
          "good": "98",
          "shopName": "中国移动京东自营官方旗舰店",
          "name": "Apple iPhone 15 Pro Max (A3108) 256GB 白色钛金属 支持移动联通电信5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/231068/39/10614/61933/6596466eF66074677/35e1e2a67f1c8926.jpg",
          "customAttrList": [
              ""
          ],
          "customAttrListNew": [
              ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "颜色",
                  "saleValue": "白色钛金属",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "存储",
                  "saleValue": "256G",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "",
                  "saleValue": "优惠一（购机立享 话费补贴）",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100038004397",
          "commentCount": "3000000",
          "price": "5499.00",
          "good": "97",
          "shopName": "Apple产品京东自营旗舰店",
          "name": "Apple/苹果 iPhone 14 (A2884) 256GB 星光色 支持移动联通电信5G 双卡双待手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/218183/20/41502/27201/6667aa4cF271bc636/8104680fe2291ec5.jpg",
          "customAttrList": [
              "A15",
              "256GB",
              "OLED直屏",
              "星光色",
              "6.1英寸"
          ],
          "customAttrListNew": [
              "A15",
              "256GB",
              "OLED直屏",
              "星光色",
              "6.1英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "星光色",
                  "sequenceNo": 2
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "公开版",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "10025627792262",
          "commentCount": "5000",
          "price": "1679.00",
          "good": "95",
          "shopName": "爱回收严选手机旗舰店",
          "name": "Apple 苹果14/13/12/11/X系列promax iPhone二手手机 详见质检报告 苹果 iPhone 12",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/229252/3/18576/98031/666725edF3ab88a9f/b4d56087eec5d200.jpg",
          "customAttrList": [
              ""
          ],
          "customAttrListNew": [
              ""
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "苹果 iPhone 12",
                  "sequenceNo": 29
              }
          ]
      },
      {
          "productId": "100066896282",
          "commentCount": "100000",
          "price": "4899.00",
          "good": "97",
          "shopName": "中国联通京东自营旗舰店",
          "name": "Apple iPhone 15 (A3092) 128GB 粉色支持移动联通电信5G 双卡双待手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/135087/9/41282/37100/65f2c671F259eb16b/0f2a271e2af689ea.jpg",
          "customAttrList": [
              "A16",
              "128GB",
              "OLED直屏",
              "粉色",
              "6.1英寸"
          ],
          "customAttrListNew": [
              "A16",
              "128GB",
              "OLED直屏",
              "粉色",
              "6.1英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "粉色",
                  "sequenceNo": 2
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "128GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "公开版",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "10024791845026",
          "commentCount": "5000",
          "price": "4879.00",
          "good": "99",
          "shopName": "鹏瑶旗舰店",
          "name": "Apple 苹果 iPhone 15 5G手机 粉色 128GB 官方标配",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/224997/27/12674/64965/65ba0bdbF18361f36/3bf97e66a332c01c.jpg",
          "customAttrList": [
              "A16",
              "128GB",
              "OLED直屏",
              "粉色",
              "6.1英寸"
          ],
          "customAttrListNew": [
              "A16",
              "128GB",
              "OLED直屏",
              "粉色",
              "6.1英寸"
          ],
          "beforeTitleUrl": "",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "粉色 128GB",
                  "sequenceNo": 5
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "10024795595979",
          "commentCount": "5000",
          "price": "4879.00",
          "good": "98",
          "shopName": "零疆旗舰店",
          "name": "Apple iPhone 15 (A3092) 支持移动联通电信5G 双卡双待手机5G手机 黑色 128GB标配",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/225203/36/18525/86540/665d7dbcF6870a8b0/21ce2575f1e96086.jpg",
          "customAttrList": [
              "A16",
              "128GB",
              "OLED直屏",
              "黑色",
              "6.1英寸"
          ],
          "customAttrListNew": [
              "A16",
              "128GB",
              "OLED直屏",
              "黑色",
              "6.1英寸"
          ],
          "beforeTitleUrl": "",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "黑色",
                  "sequenceNo": 5
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "128GB标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100038004427",
          "commentCount": "500000",
          "price": "6049.00",
          "good": "96",
          "shopName": "Apple产品京东自营旗舰店",
          "name": "Apple/苹果 iPhone 14 Plus (A2888) 256GB 星光色 支持移动联通电信5G 双卡双待手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/185338/38/47188/25198/6667ac3dF3ee27c93/ae817379c6f4f37b.jpg",
          "customAttrList": [
              "A15",
              "256GB",
              "OLED直屏",
              "星光色",
              "6.7英寸"
          ],
          "customAttrListNew": [
              "A15",
              "256GB",
              "OLED直屏",
              "星光色",
              "6.7英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "星光色",
                  "sequenceNo": 2
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "公开版",
                  "sequenceNo": 1
              }
          ]
      }
  ]
  },
  {
    "name": "小米",
    "image": "https://m.360buyimg.com/babel/jfs/t1/116172/21/35503/3634/646ed311F9c7f3497/a8555654aa2d6d07.jpg",
    "items": [
      {
          "productId": "100049486743",
          "commentCount": "500000",
          "price": "2799.00",
          "good": "96",
          "shopName": "小米京东自营旗舰店",
          "name": "小米13 徕卡光学镜头 第二代骁龙8处理器 12+256GB 黑色 5G手机 澎湃OS SU7小米汽车互联 AI手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/241787/24/10650/61586/66603516Fcead3a3c/060b556bde50bee5.jpg",
          "customAttrList": [
              "第二代骁龙8",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.36英寸"
          ],
          "customAttrListNew": [
              "第二代骁龙8",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.36英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "黑色",
                  "sequenceNo": 1
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 3
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "标准版",
                  "sequenceNo": 1
              }
          ],
          "picList": ['https://img13.360buyimg.com/n2/s270x270_jfs/t1/241787/24/10650/61586/66603516Fcead3a3c/060b556bde50bee5.jpg','https://img13.360buyimg.com/n1/s450x450_jfs/t1/244679/4/9968/60153/66506cc2Fdf731dec/8eb19f25869de637.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/201344/33/24181/70282/6395ab62E9d3e5d8e/7043ba4445ee343d.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/191767/18/32096/68036/6395ab62Ee038bbcc/e53b43b1bc4bc7a0.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/183490/33/32059/52171/6395ab62E5a6e56be/de5da2cd5cb1bafe.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/180886/39/31354/123688/6395ab62E78c504c5/c39424b34faae7ab.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/100313/10/36237/140978/6395ab62E7ff08990/c183251e6ba44544.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/188548/16/31021/92778/6395ab63E55e1e744/e893d9ea0e8458c1.jpg.avif',
          'https://img13.360buyimg.com/n1/s450x450_jfs/t1/189623/31/29818/49183/6395ab62E7ebd7ca8/7f908f65369e921e.jpg.avif']
      },
      {
          "productId": "100041367878",
          "commentCount": "200000",
          "price": "499.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米（MI）Redmi 12C Helio G85 性能芯 5000万高清双摄 5000mAh长续航 4GB+64GB 熏衣紫 智能手机 小米红米",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/199692/35/43858/82064/665da031F8711efda/5ed016b7ffea590f.jpg",
          "customAttrList": [
              "MTK G系列",
              "64GB",
              "LCD",
              "紫色",
              "6.71英寸"
          ],
          "customAttrListNew": [
              "MTK G系列",
              "64GB",
              "LCD",
              "紫色",
              "6.71英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "熏衣紫",
                  "sequenceNo": 4
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "4GB+64GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ],
          "picList": ['https://img13.360buyimg.com/n2/s270x270_jfs/t1/199692/35/43858/82064/665da031F8711efda/5ed016b7ffea590f.jpg','https://img13.360buyimg.com/n2/s450x450_jfs/t1/175638/20/32649/49677/63afa75eF9394f632/fbdec0eb84d2dab0.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/100102/22/30773/45414/63ef353dF182c06ab/728d447e8fb8a6bd.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/132958/29/29314/86364/63afa7b7Fd6273c03/069847fb354ef989.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/131990/14/29842/32449/63afa750Fd7b01d3c/3fb635a89d6e2e94.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/67044/27/24616/7134/63afa750F3ec4016a/92bc8ae39238d1ac.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/63304/22/23687/21347/63afa750Fcd4662e9/43952dcabf6d2ef2.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/206680/7/28355/23363/63afa750F479016a9/034cf33b486a41a1.jpg.avif',
          'https://img13.360buyimg.com/n2/s450x450_jfs/t1/212106/3/24701/53512/63afa750F8b2dcd5e/f6fe477fd7f9dcc4.jpg.avif']
      },
      {
          "productId": "100094231749",
          "commentCount": "50000",
          "price": "1699.00",
          "good": "98",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi Turbo 3 第三代骁龙8s 小米澎湃OS 12+256 墨晶  AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/181513/13/47747/114125/6662c83bF1e6570b2/b8debff4e8d3d96e.jpg",
          "customAttrList": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "墨晶",
                  "sequenceNo": 1
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100105742360",
          "commentCount": "50000",
          "price": "1699.00",
          "good": "98",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi Turbo 3 第三代骁龙8s 小米澎湃OS 12+256 青刃 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/178237/32/47410/106585/6662c839Fdf82cdd4/7afcb1c65cc77e40.jpg",
          "customAttrList": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "绿色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "绿色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "青刃",
                  "sequenceNo": 2
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100078020148",
          "commentCount": "200000",
          "price": "1529.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70E 天玑8300-Ultra小米澎湃OS 12GB+256GB墨羽 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/165791/18/45872/68068/665aecb3Fed87b487/ad6bbcd5c443d4bb.jpg",
          "customAttrList": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "墨羽",
                  "sequenceNo": 1
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100078020164",
          "commentCount": "200000",
          "price": "1529.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70E 天玑8300-Ultra小米澎湃OS 12GB+256GB影青 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/223810/11/34035/60899/665aecb0Fdb17401a/5d5f2db3f8ba1f90.png",
          "customAttrList": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "绿色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "绿色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "影青",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100105742422",
          "commentCount": "50000",
          "price": "1699.00",
          "good": "98",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi Turbo 3 第三代骁龙8s 小米澎湃OS 12+256 冰钛 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/246967/9/10998/100508/6662c83aF3b792f82/754e854c11332585.jpg",
          "customAttrList": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "金色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "第三代骁龙8s",
              "256GB",
              "OLED直屏",
              "金色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "冰钛",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 1
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100097767010",
          "commentCount": "500000",
          "price": "2299.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70 第二代骁龙8 澎湃OS 12GB+512GB 墨羽 红米5G手机 手机 SU7 小米汽车互联 AI手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/232020/12/20397/82819/6662ea58F8c8a6e66/f077634481939643.jpg",
          "customAttrList": [
              "第二代骁龙8",
              "512GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "第二代骁龙8",
              "512GB",
              "OLED直屏",
              "黑色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "墨羽",
                  "sequenceNo": 1
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+512GB",
                  "sequenceNo": 3
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100075799825",
          "commentCount": "200000",
          "price": "1529.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi K70E 天玑8300-Ultra小米澎湃OS 12GB+256GB晴雪 AI功能 红米5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/240597/33/9758/64660/6662b851F0e8e314e/4aa36860bbcc21a6.jpg",
          "customAttrList": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "白色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "天玑8300-Ultra",
              "256GB",
              "OLED直屏",
              "白色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "晴雪",
                  "sequenceNo": 2
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "12GB+256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      },
      {
          "productId": "100067652138",
          "commentCount": "500000",
          "price": "1299.00",
          "good": "97",
          "shopName": "小米京东自营旗舰店",
          "name": "小米Redmi Note13Pro 新2亿像素 第二代1.5K高光屏 8GB+256GB 时光蓝 SU7 5G手机",
          "imageUrl": "https://img13.360buyimg.com/n2/s270x270_jfs/t1/238512/23/6279/66761/6662b38cF60b9c492/8c31c1a9c96e58be.jpg",
          "customAttrList": [
              "第二代骁龙7s",
              "256GB",
              "OLED直屏",
              "白色",
              "6.67英寸"
          ],
          "customAttrListNew": [
              "第二代骁龙7s",
              "256GB",
              "OLED直屏",
              "白色",
              "6.67英寸"
          ],
          "beforeTitleUrl": "https://m.360buyimg.com/umm/jfs/t1/229796/17/15358/2054/664c0a12Fafc90bf6/902f91c17e32c8b4.png",
          "saleAttributes": [
              {
                  "dim": 1,
                  "saleName": "外观",
                  "saleValue": "时光蓝",
                  "sequenceNo": 3
              },
              {
                  "dim": 2,
                  "saleName": "版本",
                  "saleValue": "8GB+256GB",
                  "sequenceNo": 2
              },
              {
                  "dim": 3,
                  "saleName": "购买方式",
                  "saleValue": "官方标配",
                  "sequenceNo": 1
              }
          ]
      }
  ]
  },
  {
    "name": "华为",
    "image": "https://m.360buyimg.com/babel/jfs/t1/6075/28/15456/6357/646ed3f4Ff1fd964c/131e82d10898c629.jpg"
  },
  {
    "name": "荣耀",
    "image": "https://m.360buyimg.com/babel/jfs/t1/194262/34/35753/2551/646ed8f2F8b9bc809/b771ffaa347da975.jpg"
  },
  {
    "name": "OPPO",
    "image": "https://m.360buyimg.com/babel/jfs/t1/174915/5/36279/16652/646ed83fFa4ad8415/e685c71e4594bf5a.png"
  },
  {
    "name": "一加",
    "image": "https://m.360buyimg.com/babel/jfs/t11209/71/2351231464/4158/fc0c415c/5a16923fNfc14307e.jpg"
  },
  {
    "name": "vivo",
    "image": "https://m.360buyimg.com/babel/jfs/t1/155687/16/37363/2969/646edb20Faf68cc6d/2531a2894ea169c8.jpg"
  },
  {
    "name": "三星",
    "image": "https://m.360buyimg.com/babel/jfs/t1/137201/37/36913/2566/646edbd8Fce7336d4/178c7beb42b9feea.jpg"
  },
  {
    "name": "魅族",
    "image": "https://m.360buyimg.com/babel/jfs/t1/55698/9/23169/2309/646edca2F683afc66/712c59bd8a2cb41f.jpg"
  },
  {
    "name": "realme",
    "image": "https://m.360buyimg.com/babel/jfs/t1/208171/6/32248/5080/646ef98eF876d32f1/dfa7defc2893496b.jpg"
  },
  {
    "name": "努比亚",
    "image": "https://m.360buyimg.com/babel/jfs/t1/57943/27/21537/3088/646efb75F15f2c4ce/65dee99a32265564.jpg"
  },
  {
    "name": "摩托罗拉",
    "image": "https://m.360buyimg.com/babel/jfs/t1/59757/3/20689/3020/646efc27F801b6bfa/34691b6405f5e210.jpg"
  }
  ]],
  ["休闲食品", [{
    "name": "糖果",
    "image": "https://m.360buyimg.com/babel/jfs/t1/215072/20/30644/9019/646da7a1F63613a8c/a5b2428860e14917.jpg"
  },
  {
    "name": "辣条豆干",
    "image": "https://m.360buyimg.com/babel/jfs/t1/100342/15/41391/10528/646dad13F717b6cd9/a301d60c251994d8.jpg"
  },
  {
    "name": "零食大礼包",
    "image": "https://m.360buyimg.com/babel/jfs/t1/124135/17/31878/12612/64647e4aF3c9a994c/3ad53a09368ed16f.png"
  },
  {
    "name": "巧克力",
    "image": "https://m.360buyimg.com/babel/jfs/t1/114477/21/36707/8373/646da8d6Facec2806/fd8d1f9c0bb1bc5a.jpg"
  },
  {
    "name": "海味零食",
    "image": "https://m.360buyimg.com/babel/jfs/t1/163882/11/37490/6621/6465e34dF9347b277/62f8a1462d39513a.jpg"
  },
  {
    "name": "凤爪卤味",
    "image": "https://m.360buyimg.com/babel/jfs/t1/180440/40/30976/11445/6386f8fdEd5f4573d/c997bf23155480e7.png"
  },
  {
    "name": "果冻布丁",
    "image": "https://m.360buyimg.com/babel/jfs/t1/113605/35/30715/9589/6386f8fdE96e0a1a5/0ec1901933d934d3.png"
  },
  {
    "name": "低卡零食",
    "image": "https://m.360buyimg.com/babel/jfs/t1/51031/14/17648/10793/6386f8fcEa5576b6f/19f261d5e5e7074b.png"
  },
  {
    "name": "进口零食",
    "image": "https://m.360buyimg.com/babel/jfs/t1/113450/2/34014/13767/63871eb1E3e3cce39/93afddc37745823f.png"
  },
  {
    "name": "坚果炒货",
    "image": "https://m.360buyimg.com/babel/jfs/t1/89600/33/15817/11753/5e747e68E43e789b9/15bff8b49fba2944.jpg"
  },
  {
    "name": "饼干糕点",
    "image": "https://m.360buyimg.com/babel/jfs/t1/211229/35/28223/12334/6386f8fcE70a8ff11/e9227f4f87e7535a.png"
  },
  {
    "name": "薯片膨化",
    "image": "https://m.360buyimg.com/babel/jfs/t1/183392/2/31795/9391/6386f8fdE5661856e/7b25c26f7460d29a.png"
  }
  ]],
  ["中式厨电", [{
    "name": "炒菜机器人",
    "image": "https://m.360buyimg.com/babel/jfs/t1/92370/3/40455/5787/646dd448Fb4a0ed9f/6d95d4dab252795f.jpg"
  },
  {
    "name": "豆浆机",
    "image": "https://m.360buyimg.com/babel/jfs/t1/122241/23/38000/1965/646dd3c6Fb462b270/9054aeedc4e1275c.jpg"
  },
  {
    "name": "电煮锅",
    "image": "https://m.360buyimg.com/babel/jfs/t1/149170/35/32999/3011/646dd3b5Fdcdedcda/37f7ad0cc8e0f9ba.jpg"
  },
  {
    "name": "电饼铛",
    "image": "https://m.360buyimg.com/babel/jfs/t1/222918/8/22131/3371/646dd380Ff9a55b2a/c43abc9e5585f4d1.jpg"
  },
  {
    "name": "电磁炉",
    "image": "https://m.360buyimg.com/babel/jfs/t1/217854/10/27047/1653/646dd370Fc2020081/be1066dbe34de67c.jpg"
  },
  {
    "name": "电压力锅",
    "image": "https://m.360buyimg.com/babel/jfs/t1/109604/21/39223/5261/646dd330F3270ea24/1409c78ffa64f059.jpg"
  },
  {
    "name": "养生壶",
    "image": "https://m.360buyimg.com/babel/jfs/t1/61342/26/20996/3546/646dd30cF80f6e564/f8b499876fbd0505.jpg"
  },
  {
    "name": "多功能锅",
    "image": "https://m.360buyimg.com/babel/jfs/t1/210681/9/33050/3701/646dd2fdF95317ed3/b9c679eae5c55be9.jpg"
  },
  {
    "name": "电热水壶",
    "image": "https://m.360buyimg.com/babel/jfs/t1/80005/40/23732/2573/646dd2ddF93793ff4/617a8257b6a15517.jpg"
  },
  {
    "name": "电饭煲",
    "image": "https://m.360buyimg.com/babel/jfs/t1/196882/27/33675/2336/646dd2c4Fc66cfb75/7aeb540fc01fb876.jpg"
  }
  ]],
  ["纸品湿巾", [{
    "name": "湿巾",
    "image": "https://m.360buyimg.com/babel/jfs/t1/102690/15/34886/3783/6464a09bFf0335d8d/81ed6c2e05782779.jpg"
  },
  {
    "name": "手帕纸",
    "image": "https://m.360buyimg.com/babel/jfs/t1/51730/31/22360/3723/6464a09eF93d52055/dcf3c83e0c72522c.jpg"
  },
  {
    "name": "厨房纸巾",
    "image": "https://m.360buyimg.com/babel/jfs/t1/84242/7/18029/4061/6386e2d4E4a46dba6/3b75ac754c2b317e.jpg"
  },
  {
    "name": "湿厕纸",
    "image": "https://m.360buyimg.com/babel/jfs/t1/162026/21/34577/4142/6464a0a0F91ef96e7/a22953dbb37ec06e.jpg"
  },
  {
    "name": "抽纸",
    "image": "https://m.360buyimg.com/babel/jfs/t1/199781/33/34819/4132/64649ebcFb627f5f5/5338c460952c00cb.jpg"
  },
  {
    "name": "卷纸",
    "image": "https://m.360buyimg.com/babel/jfs/t1/175461/4/37670/5123/64649ebeFa9c7c571/2cd3a88e2e9a7632.jpg"
  }
  ]],
  ["女士上衣", [{
    "name": "卫衣",
    "image": "https://m.360buyimg.com/babel/jfs/t1/65579/18/26507/5394/6465cdbdF9132cdd0/c813c35ca9500df3.jpg"
  },
  {
    "name": "针织衫",
    "image": "https://m.360buyimg.com/babel/jfs/t1/216457/11/21629/2132/6388e999E8ce79687/978ced6fc84fa089.jpg"
  },
  {
    "name": "衬衫",
    "image": "https://m.360buyimg.com/babel/jfs/t1/115463/35/33775/6960/6465ce62F8cbdf69c/fcd8d9cf0c7f1633.jpg"
  },
  {
    "name": "毛衣",
    "image": "https://m.360buyimg.com/babel/jfs/t1/219275/7/21915/7299/6465cffdF5254bb41/4ad82c0f9f1a2a96.jpg"
  },
  {
    "name": "马甲",
    "image": "https://m.360buyimg.com/babel/jfs/t1/199721/29/34753/2263/6464a123F34fc969a/c6ed9bbfb8888f7c.jpg"
  },
  {
    "name": "雪纺衫",
    "image": "https://m.360buyimg.com/babel/jfs/t1/140758/30/33716/7362/643e114eFc2611740/d928bc03a468ac56.jpg"
  },
  {
    "name": "小背心",
    "image": "https://m.360buyimg.com/babel/jfs/t1/185505/1/33998/2120/641bfda0Fa996fe6a/a3162ae3ff490aa8.jpg"
  },
  {
    "name": "轻薄打底",
    "image": "https://m.360buyimg.com/babel/jfs/t1/7317/20/20732/2256/641bfdf4Fb58ef3e5/e35e936c85dda3f8.jpg"
  },
  {
    "name": "T恤",
    "image": "https://m.360buyimg.com/babel/jfs/t1/212425/38/29895/3620/6465cd72F45b9bc98/b608871c4792c087.jpg"
  },
  {
    "name": "凉感T恤",
    "image": "https://m.360buyimg.com/babel/jfs/t1/101643/15/33602/1832/643e0a61F571bce66/2bd390dec9adafec.jpg"
  },
  {
    "name": "条纹衬衫",
    "image": "https://m.360buyimg.com/babel/jfs/t1/192575/40/32954/1868/641bfceaFa4aae433/d8489b2a4500f06b.jpg"
  },
  {
    "name": "泡泡袖上衣",
    "image": "https://m.360buyimg.com/babel/jfs/t1/203324/32/33495/2579/643e10faFfe75e636/acc351b4470961fa.jpg"
  }
  ]]
])