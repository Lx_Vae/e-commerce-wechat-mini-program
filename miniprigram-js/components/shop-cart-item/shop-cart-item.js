// components/shop-cart-item/shop-cart-item.ts
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    showCheckbox: {
      type: Boolean,
      value: true
    },
    goods: {
      type: Object,
      value: {
        image: 'https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0',
        name: '默认名称',
        price: '0',
        checked: false
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    checkChange(e) {
      this.setData({
        ['goods.checked']: e.detail
      }, () => {
        this.triggerEvent('selectChange', { selected: e.detail, goods: this.data.goods })
      })
    },
    toGoodsDetailView(e) {
      wx.navigateTo({
        url: `/pages/goods-detail/goods-detail?data=${JSON.stringify(this.data.goods)}`
      })
    }
  }
})