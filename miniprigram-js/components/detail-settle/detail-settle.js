// components/detail-settle/detail-settle.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toShopCartView() {
      this.triggerEvent('toShopCartView')
    },
    addShopCart() {
      this.triggerEvent('addShopCart')
    },
    buyNow() {
      this.triggerEvent('buyNow')
    }
  }
})