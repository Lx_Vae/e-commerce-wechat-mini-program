// components/settlement/settlement.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    selectedCount: 0,
    totalPrice: 0,
    allChecked: false
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    checkAll(e) {
      this.setData({
        allChecked: e.detail,
      }, () => {
        this.triggerEvent('checkAll', e.detail)
      });
    }
  }
})