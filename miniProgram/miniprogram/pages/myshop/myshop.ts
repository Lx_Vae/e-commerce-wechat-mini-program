import { mediaProps } from "miniprogram/miniprogram_npm/@vant/weapp/uploader/shared";

// pages/myshop/myshop.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: [],
    sidebarList: [],
    goodsMap: new Map(),
    curGoodsList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.initDefaultData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    console.log('MyShop 页面刷新成功');
    setTimeout((): void => {
      wx.stopPullDownRefresh()
    }, 500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  initDefaultData() {
    wx.request({
      url: 'http://123.57.4.50:8088/miaomiao/getitems',
      method: 'GET',
      success: ({ data }) => {
        data = new Map(Object.entries(data.data))
        const siderbarList: Array<string> = [...data.keys()]
        for (const siderbar of siderbarList) {
          const goodsNameList = data.get(siderbar)
          const goodsList = goodsNameList.map((goodsName) => {
            return {
              image: 'https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0',
              name: goodsName,
              price: 100
            }
          })
          data.set(siderbar, goodsList)
        }
        this.setData({
          swiperList: [
            'http://kwxy.jsnu.edu.cn/_upload/article/images/52/43/63006d404d439cc520138e7a961e/b442831a-636c-4db5-9c3d-6a1cbdca8d4b.jpg',
            'http://kwxy.jsnu.edu.cn/_upload/article/images/52/43/63006d404d439cc520138e7a961e/854e8d9e-fbde-4ff3-ae30-8addb89fc552.jpg'
          ],
          sidebarList: siderbarList,
          goodsMap: data,
          curGoodsList: data.get(siderbarList[0])
        })
      }
    })
  },
  siderHandler(e) {
    const index = e.currentTarget.dataset.index
    this.setData({
      curGoodsList: this.data.goodsMap.get(this.data.sidebarList[index])
    })
  },
  toGoodsDetailView(e) {
    const index = e.currentTarget.dataset.index
    const data = this.data.curGoodsList[index]
    wx.navigateTo({
      url: `/pages/goods-detail/goods-detail?data=${JSON.stringify(data)}`
    })
  }
})