// pages/shop-cart/shop-cart.ts
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    selectedList: [],
    totalPrice: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.initData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  initData() {
    this.setData({
      goodsList: [
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单0", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单1", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单2", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单3", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单4", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单5", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单6", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单7", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单8", price: 100 },
        { image: "https://wx.qlogo.cn/mmhead/Q3auHgzwzM5AEEsPGibXP08VtslysPQ3rrIctiaq1NDgqKDfrpD6U9lw/0", name: "T恤子菜单9", price: 100 }
      ]
    })
  },
  selectChange(e) {
    console.log(e.detail);
    const goods = e.detail.goods
    let newSelectedList
    let newTotalPrice
    if (e.detail.selected) {
      this.data.selectedList.push(goods)
      newSelectedList = this.data.selectedList
      newTotalPrice = this.data.totalPrice + goods.price
    } else {
      newSelectedList = this.data.selectedList.filter(g => {
        return g.name != goods.name
      })
      newTotalPrice = this.data.totalPrice - goods.price
    }

    this.setData({
      totalPrice: newTotalPrice,
      selectedList: newSelectedList
    })
  }
})